
// Blob start___________________________________________________________________
class Blob {
  constructor(_x,_y,_sizeX, _sizeY){
this.x = _x;
this.y = _y;
this.sizeX = _sizeX;
this.sizeY = _sizeY;
}


  showBall(){
    if (start === 0){
      image(introimg, 0, 0)
      image(ballGif, this.x, this.y-100, this.sizeX, 420);
      push();
       fill(230);
       textSize(40);
       textAlign(CENTER);
       text('What is the name of your friend?',448,565);
      pop();

    }
  }

  playJump(){
      image(jumpGif, this.x, this.y-100, this.sizeX, 420);
  }





  // moods start _______________________________________________________________
  idle(){
    //Mood = 0
    image(idleGif, this.x, this.y, this.sizeX, this.sizeY);
  }

  idle2(){
    //Mood = 0 + level 3
    image(idle2Gif, this.x, this.y, this.sizeX, this.sizeY);
  }

  idle3(){
    //Mood = 0 + level 4
    image(idle3Gif, this.x, this.y, this.sizeX, this.sizeY);
  }

  hungry(){
    //Mood = 1
    image(hungryGif, this.x, this.y, this.sizeX, this.sizeY);
  }

  satisfied(){
    //Mood = 2
    image(satisfiedGif, this.x, this.y, this.sizeX, this.sizeY);
    image(rainbowGif, this.x + 25, this.y + 20, 220, 220)
    if (bubbleTimer > 4) {
      speBub.hide();
      mood = 0;
      bubbleTimer = 0;
      poopTimer = 0;
    }
  }

  satisfiedFood(){
    //Mood = 2 + fed = true
    image(satisfiedGif, this.x, this.y, this.sizeX, this.sizeY);
    image(rainbowGif, this.x + 25, this.y + 20, 220, 220)
    image(foodBowlGif, 280, 310, 150, 150);
    if (bubbleTimer > 4) {
      speBub.hide();
      mood = 0;
      bubbleTimer = 0;
      poopTimer = 0;
      fed = false;
    }
  }

  cryAlot(){
    //Mood = 3
    image(cryGif, this.x, this.y, this.sizeX, this.sizeY);
    speBub.show();
    icon.showPoop();
  }

  cryAlittle(){
    //Mood = 4
    image(cryGif, this.x, this.y, this.sizeX, this.sizeY);
    if (cryTimer < 5) {
      speBub.show();
      icon.showDeath();
    }
    if (cryTimer > 4) {
      mood = 0;
      resetTimers();
    }
  }

  doNotWant(){
    //Mood = 5
    if(noTimer < 2){
    image(noGif, this.x, this.y, this.sizeX, this.sizeY);
  }
    if (noTimer > 1) {
      mood = 0;
  }
}

  pet(){
    //Mood = 6
    image(petGif, this.x-60, this.y, this.sizeX, this.sizeY);
    if (petTimer > 2) {
      mood = 0;
    }
  }

  die(){
    //Mood = 7
    image(deadGif, this.x + 50, this.y);
  }
  // moods end _________________________________________________________________



  // These next function are for displaying the current health of the blob
  healthGreat(){
    //health = 3
      image(heartFullImg, 834, 15, 50, 50);
      image(heartFullImg, 774, 15, 50, 50);
      image(heartFullImg, 714, 15, 50, 50);
  }
  healthGood(){
    //health = 2
    image(heartFullImg, 834, 15, 50, 50);
    image(heartFullImg, 774, 15, 50, 50);
    image(heartEmptyImg, 714, 15, 50, 50);
  }
  healthBad(){
    //health = 1
    push();
    image(heartFullImg, 834, 15, 50, 50);
    image(heartEmptyImg, 774, 15, 50, 50);
    image(heartEmptyImg, 714, 15, 50, 50);
  }
  healthDead(){
    //health = 0
    image(heartEmptyImg, 834, 15, 50, 50);
    image(heartEmptyImg, 774, 15, 50, 50);
    image(heartEmptyImg, 714, 15, 50, 50);
  }

}
// Blob end ____________________________________________________________________



// TextCursor start ____________________________________________________________
class TextCursor {
  constructor(_x, _y){
    this.x = _x;
    this.y = _y;
    this.xSpeed = 0;
    this.ySpeed = 0;
  }

  show(){
    //draws the cursor on screen
    if (startAskData === false){
    push();
      noFill();
      stroke(230);
      strokeWeight(5);
      rectMode(CENTER);
      rect(this.x, this.y, 180, 50);
    pop();
  }
    if (startAskData === true) {
      this.x = 220;
    }
  }

  dir(_x, _y){
    //recieves input to allow movement in "move()"
    this.xSpeed = _x;
  }

  move() {
    //Allows for the cursor to move
    this.x = this.x + this.xSpeed;
    this.x = constrain(this.x, 220, 660);
  }

  check(){
    //This checks what option is selected/highlighted

    if (this.x === 220){
      selection = 1;
    } else if (this.x === 440){
      selection = 2;
    } else if (this.x === 660){
      selection = 3;
    }
  }

}
// TextCursor end ______________________________________________________________



// Word start _______________________________________________________________
class Word {
  constructor(_x, _y, _word){
    this.x = _x;
    this.y = _y;
    this.word = _word;
  }

  show(){
    if (startAskData === false){
    push();
      fill(230);
      textSize(55);
      textAlign(CENTER);
      text(this.word, this.x, this.y);
    pop();
    }
  }

  showName(){
    if (startAskData === false){
    push();
      fill(230);
      textSize(50);
      text(this.word, this.x, this.y);
    pop();
    }
  }

}
// Word end ____________________________________________________________________



// SpeechBubble start __________________________________________________________
class SpeechBubble {
  constructor(_x, _y, _size){
    this.x = _x;
    this.y = _y;
    this.size = _size;
    this.reset = _x;
  }

  show(){
    this.x = this.reset;
    image(speechImg, this.x, this.y, this.size, this.size);
  }

  hide(){
    this.x = -100;
  }


}
// SpeechBubble end _____________________________________________________________



// Icon start __________________________________________________________________
class Icon {
  constructor(_x, _y, _size){
    this.x = _x;
    this.y = _y;
    this.size = _size;
  }

  showHungry(){
    image(hungryIconGif, this.x, this.y, this.size/2, this.size);
  }

  showDeath(){
    image(skullGif, this.x-14, this.y + 55, this.size -40, this.size -40);
  }

  showPoop(){
    image(poopImg, this.x - 30, this.y+36, this.size-40, this.size-40);
    image(panic1Img, this.x + 20, this.y+65, this.size-70, this.size-70);
  }
}
// Icon end ____________________________________________________________________



// FoodTimerBar start __________________________________________________________
class FoodTimerBar {
  constructor(_x, _y, _time){
    this.x = _x;
    this.y = _y;
    this.time = _time;
  }

  show(){
    push();
      push();
        stroke(230);
        strokeWeight(3);
        fill(111, 127, 159);
        rect(this.x, this.y - 300, 76, 300);
        rect(this.x, this.y - 330, 76, 30);
      pop();
      push();
          fill(230);
          textSize(20);
          text('FEED?', this.x + 6, this.y - 308);
          stroke(230);
          strokeWeight(3);
          fill(181, 30, 30);
          rect(this.x, this.y, 76, -10 * this.time);
      pop();
    pop();
  }
}
// FoodTimerBar end ____________________________________________________________



// Poop start __________________________________________________________________
class Poop {
  constructor(_x, _y, _size){
    this.x = _x;
    this.y = _y;
    this.size = _size;
  }

  show(){
    image(poopImg, this.x, this.y, this.size, this.size);
  }
}
// Poop end ____________________________________________________________________
