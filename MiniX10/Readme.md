Screenshot of the "Personality-test" flowchart, made in miro
<br>
<img src="personalitytest.jpg" width=500>

Screenshot of the "feed it with your data" flowchart, made in miro
<br>
<img src="datacat.jpg" width=700>

Screenshot of the "flappy bird" flowchart, made in Axcure RP 9
<br>
<img src="flappybird.png" width=500>


**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?** 


What I found hard in my personal flowchart is to find a balance of what is understandable. I had to some sort of decide how technical should it be before I made the flowchart. A flowchart can be technical and very hard to understand If you make them with all details. So, if you were showing the flowchart to a non-coder or at least one with a decent knowledge of code they will never understand your flowchart. I tried to put myself into someone shoes and decided to use Google to find a very technical flowchart from another language and I was lost. Before I made the program, I decided to make a flowchart there is possible for a coder to read but at the same time a non-coder had a slight chance to understand it. My program was inspired by the game “Flappy Bird” and most of the people today who is using a smartphone knows the game and knows its rules in general. I know there is older people and others who never plays games on their phones, but if they don’t do this I am pretty sure they will never look at a flowchart.  
The end product is not over technical but it still got some basic elements from coding, so maybe it is not the best flowchart for a programmer to read but other people can understand it as well.  


**What are the technical challenges facing the two ideas and how are you going to address these?**


“Feed it with your data”
<br>
The challenge with this program from my point of view is the way we capture and store the data the user feeds the cat with. We had some thoughts about storing it in a JSON.file. Another difficult part of the program is that we want the data to be usable to feed the cat, but the twist is the data can only be used once. This means the cat needs new data every time to stay alive, so you cannot give the machine your personal ID 2 times. For me personally this seems like a tricky part of the program. I am sure we can manage to make it, my group are filled with coding talent and creative heads.  


“Personality-test”
<br>
The tricky part of this program is to make the two elements of the game. The first part of the program is an algorithm that can solve your placement in a social hierarchy from the answers the user gives the program. The second part is a game that is executed when the algorithm has placed you in the hierarchy. However, making both programs individually does not seem like a challenge for us but I think trying to mix them together might be a little bit harder, I have not made anything like that and neither does my buddy-group members. To come up with these two “blueprints” of the ideas we used brainstorming methods from last semesters CO-Design. I think we might need a few more sessions to sit down and make some brainstorming in how we should make these programs and how will we delegate the different jobs.


**In which ways are the individual and the group flowcharts you produced useful?**


One of my co-students in the group came up with an idea to vote for the AP themes we wanted to work with, and after we found those themes we brainstormed for ideas, discussions etc. When you sit and brainstorm and all the ideas are drawn on a board you sometimes lose the overview, but here the flowchart came in handy for me. The two ideas we came up in the group was not ideas I came up with, but with the flowcharts it made it easier for me to sort out what the programs were about. We started making a prototype-flowchart and then we could just bring in ideas to edit our program/flowchart. This resulted that we made four flowcharts and the last two of the was our result. I liked the way we build up the program from our brainstorm and then ended up with an understandable flowchart to describe what we wanted to do with the program. As I mention earlier for me personally it helped me a lot to understand how my co-workers was thinking about the program and how it should run and how you interact with it. 
My own flowchart is not the most technical or flashy flowchart, but it is also a simpler program than the ones we were trying to make in the group so I think it makes sense. The difference for me is that I made the program some weeks ago so I had to go back and rerun the program to remember it. I remember the program to be much more tricky and technical, but when I tried to make some flowcharts of my program I was a little bit shocked. When I made the flappy bird game I had a lot of troubles running the game, and I think therefor I think my preconception of the flowchart was that it will end up being technical and hard to make. However, this was not the case I found my program easy to convert to a flowchart version. 
For me both my own and the group made flowcharts was helpful for me, either if it was showing me some basics of the program I am doing together with my group, or how it showed me that some tricky code can actually end up being a very simple program. The last thing might be the one I found least helpful, but it was somehow funny to see that tricky code is not equals a tricky program. 

