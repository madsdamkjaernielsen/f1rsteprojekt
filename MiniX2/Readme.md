[:)Link to my project:)](https://madsdamkjaernielsen.gitlab.io/f1rsteprojekt/MiniX2/)
<br>
<img src="ScreenSavetMinix2.png" width=600>
### MiniX2
My starting point with this program is the text: _"Executing Practices"_, to be more precise I worked with the part: _"Modifying the universal"_ written by Roel Roscam Abbing, Peggy Pierrot and Femke Snelting. In this paper, they talk about how emojis turned out to be such a complex thing to adjust to the global changing environment. As they mention in the paper:

<br>
_"We can only wonder how this will be further used in a changing political context where cultural or "ethnic" profiling of Internet users has become normal. Superficial colour-blindness abounds while a wide wave of reactionary movements"_ (p.50).

<br>

They are worried about how will the emoji culture be able to adjust under these circumstances, because they are in the middle of a global political question mark. We see big companies like google, apple etc. trying to be as multiple and neutral as possible. But like in the paper it is impossible to please everyone, and how can we avoid overriding someone. 

<br>

From my program I learn to use new syntaxes as the rotate(), translate() and the double setup “push() and pop()”. The rotate was the hardest to master because you need to declare some variables and specify are you using “DEGREES” or another angle value. At the same time you need to use the command “Translate()” to be able to move the origin “(0,0)” to the certain spot you want your object to turn around. 

<br>

Throughout my process, I was thinking how can I make a program with emojis. One of the things I thought about is a global problem these days on the social medias. Often, we see influencers or celebrities trying to show us the PERFECT life! You can argue they are showing a wrong ideal image of a normal person’s life. My idea with this code was to create a picture of 3 persons with the exact the same look, I tried to simulate how people yearn to look like someone else or want to be another person. (worst case: we might end up looking like the same person everyone).       However, I am a huge fan of LEGO <3 and especially minifigures, there are endless of ways to customize a minifig. So, they fit perfect into my project. The LEGO heads will link as the faces of people using loads of filter on Instagram etc. 
<br>
I am not sure but I never had an LEGO minifigure who had any form of handicap, I mean the individual minifig might have had a flaw. However, I don’t think LEGO made an minifigure with a handicap on purpose. This might be weird. I am aware of minifigs not having feelings or any sign of life. But how do a LEGO mini fig look if it was blind, deaf or epileptic. I don’t know, but I tried to make something like that. So, I added 3 minifig heads and I named that the “the ideal image” this is supposed to simulate how we want to look with filters on Social Medias. If you then press any key on the keyboard you see the behind “the ideal image” filter. So, the code makes it possible to remove peoples filter, and you will be able to see the flaws somebody has even though they use filters. This project has no critic to LEGO at all, but I was wondering if we need to change emojis to fit into today political movement, maybe one day LEGO minifigs needs to do the same.  This point is a bit abstract but for me personal it a little bit funny to think about. 

**reference** : 
<br>
- Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016
