let img;
let angle = 0
function preload() {
  img = loadImage('legohead1.png');
}

function setup() {
  //put setup code here
createCanvas(800,600);
angleMode(DEGREES);


}
function draw() {

  // put drawing code here
  background(35);
  textSize(40)
  fill(255)
  text("Hold any key to remove ''the ideal image''!",50,50)
  image(img,100,100, 200, 200)
  image(img,400,100, 200, 200)
  image(img,250,350, 200, 200)

//if key is pressed see the magic
  if (keyIsPressed) {
//spot
push();
noStroke();
fill(255,0,0);
ellipse(535,250,6,6);
pop();

//tongue
push();
translate(346,490);
  rotate(angle);
translate(0,-15);
  fill(255,0,0);
  //noStroke()
  strokeWeight(1);
  rectMode(CENTER);
  rect(0,0,25,55,15);
  angle = angle +3;
pop();

//klap for øjet
push();
strokeWeight(4);
line(128,155,276,220);
fill(0);
rect(210,185,29,29,10);
fill(255,0,0)
noStroke();
fill(0);
triangle(490, 180, 501, 190, 510, 180);

//mono
noFill();
stroke(0, 0, 0);
strokeWeight(8);
bezier(530, 185, 550, 180, 420, 180, 480, 180);
rect(505,228,2,5);

pop();




}
}
