[:) My  'Throbber' project :)](https://madsdamkjaernielsen.gitlab.io/f1rsteprojekt/MiniX3/)
<br>
[**Link to my code**](https://gitlab.com/madsdamkjaernielsen/f1rsteprojekt/-/blob/master/MiniX3/sketch.js)
<br>
<img src="X-Wing_Top-removebg-preview.png" width=50>
<br>
<img src="minix3ss.png" width=600>


For this week, I want to somehow explore a throbber there is very close to my interest and I haven’t seen before. I linked my throbber up to the movie series Star Wars and I choose a very popular aircraft called an ‘X-Wing Starfighter’ usually used by the Rebel alliance and the Resistance in the original movies. I got my inspiration from the DSB app you use to buy train ticket from. If you see the picture I reference till, it shows a train going around the tracks around a center point. Somehow, I wanted to implement this in my own sketch but with some more Sci-Fi space theme. So, I made an X-wing flying around the same spot in an infinite loop. Else I put up two meteorites flying along with the Starfighter but not in the same sync. This is in a way a little bit frustrating, but also at the same time a little bit funny to watch because it makes the program look a little bit unfinished or the movement seems disturbed. However, they are all rotating around the same point, but they just need to cover different distances.  
<br>
I used a lot of different techniques to make the background, I wanted it to be loads of stars appearing random on the screen every time you reload the site. I recommend you trying to refresh the page to see the stars appear random everytime. Anyways, I tried many ways and if you look in my code there is plenty of parts commented out of the code, some of them might work or not. I just let them stay there to show you how many steps it actually took to end up with a solution, and I am still not sure I did it the smartest way. I ended up with a for loop combined with an array so I could make the for loop decide the random placement of the stars. I never found out how to spawn multiple stars so I decided to copy paste the same syntax and start to plus and minus the random (x,y) positions. 
To make the X-wing and the meteorite fly around somewhere else than the origin (0,0) I used the syntax ‘Translate(xx,xx)´ to change the point of origin, this made the aircraft and meteorites circulate a round a new origin. To make sure the aircraft was angled the right way I decided to close the part of the code in a push() and pop() to make sure im only changing things in this part of the code. In this part, I made another translate point to some sort of rotating the X-wing a little bit so it was flying around the origin without the plane pointing in a weird direction. For the bacground i changed its opacity to make the X-wing fade a little bit in the picture, this makes it looks like it is flying with very high speed in space. This is a edit i like a lot even though it is a veri simple code i just set the background to background(0,70) so it had 70x opacity... *credits to Christian from my bodygroup* 
<br>

This throbber does not show much information for the spectator, not like the DBS train throbber I referred to. The DSB throbber disappears when it is done loading train departures or etc. but the throbber I made does not give anything infomation, because it loops repeatedly. The X-wing throbber won’t tell you if there is 10 seconds left in an action or If there I 1 hour. If it was used anywhere else for example in a loading screen for a game or anything it could show the loading time and it will disappear when the loading is over, and another screen in the game will appear. How the program is at the moment, it is not showing anything but it could be a prototype or example of a loading icon in the future.  
<br>
<br>
**references:**
<br>
The Coding Train: Daniel Shiffman
<br>
4.1: while and for loops https://www.youtube.com/watch?v=cnRD9o6odjk&t=666s&ab_channel=TheCodingTrain
<br>
7.1: What is an array https://www.youtube.com/watch?v=VIQoUghHSxU&ab_channel=TheCodingTrain
<br>
7.2: Arrays and Loops  https://www.youtube.com/watch?v=RXWO3mFuW-I&ab_channel=TheCodingTrain
<br>
DSB train Throbber:
<br>
<img src="dsbtrain.PNG" width=300>
