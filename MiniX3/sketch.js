var nums = [5,5,5,5];
let angle = 0;
let img;
let randomx
let randomy
var pos



function preload() {
  img = loadImage('X-Wing_Top-removebg-preview.png');

}
function setup() {
  // put setup code here
createCanvas(800,600);
angleMode(DEGREES);
frameRate(60);
randomx = random(0,100);
randomy = random(0,100);


}
function draw() {
  let num = 200;
  // put drawing code here
  //Center dot
background(0,70);
fill('white');
//ellipse(randomx,randomy,9,9);
noStroke();
//center
//ellipse(400,300,5,5);

//for loop array
for (var i = 0; i < 4; i++) {
stroke(255);
ellipse(randomx,randomy,nums[i],nums[i]);
ellipse(randomx+randomx,randomy+randomy,nums[i],nums[i]);
ellipse(randomx+50,randomy+50,nums[i],nums[i]);
ellipse(randomx+230,randomy+222,nums[i],nums[i]);
ellipse(randomx+80,randomy+500,nums[i],nums[i]);
ellipse(randomx+222,randomy+23,nums[i],nums[i]);
ellipse(randomx+444,randomy+70,nums[i],nums[i]);
ellipse(randomx+120,randomy+50,nums[i],nums[i]);
ellipse(randomx+600,randomy+20,nums[i],nums[i]);
ellipse(randomx+100,randomy+50,nums[i],nums[i]);
ellipse(randomx+200,randomy+100,nums[i],nums[i]);
ellipse(randomx+300,randomy+200,nums[i],nums[i]);
ellipse(randomx+400,randomy+300,nums[i],nums[i]);
ellipse(randomx+500,randomy+400,nums[i],nums[i]);
ellipse(randomx+600,randomy+200,nums[i],nums[i]);
ellipse(randomx+10,randomy-80,nums[i],nums[i]);
ellipse(randomx-200,randomy-150,nums[i],nums[i]);
ellipse(randomx-30,randomy-600,nums[i],nums[i]);
ellipse(randomx-500,randomy-500,nums[i],nums[i]);
ellipse(randomx+44,randomy+44,nums[i],nums[i]);
ellipse(randomx+90,randomy+400,nums[i],nums[i]);
ellipse(randomx+400,randomy+90,nums[i],nums[i]);
ellipse(randomx+800,randomy+21,nums[i],nums[i]);
ellipse(randomx+30,randomy+90,nums[i],nums[i]);


}


//x-wing
push();
translate(400,300);
rotate(angle);
translate (50,-90);
//imageMode(CENTER);
image(img,50,50,90,90);

//2*exhaust
stroke(255,255,0)
strokeWeight(2)
line(84,40,87,52);
line(101,40,104.5,52);
angle = angle + 1.5;
pop();

push();
//for(i=0;i<360;i=i+50){
  let conf = 360/num*(frameCount%num);

//metorite
//for(i = 0; i < 360; i++);
translate(400,300);
rotate(conf*-1);
noStroke();
fill(255,255,20);
ellipse(200,20,10);
pop();

push();
//for(i=0;i<360;i=i+50){
  let conf1 = 360/num*(frameCount%num);

//for(i = 0; i < 360; i++);
translate(400,300);
rotate(conf*1);
noStroke();
fill(255,255,20);
ellipse(85,20,10);
pop();



//Stars
/*
fill(255,255,255)
ellipse(10,10,5);
ellipse(30,30,5);
ellipse(30,5,5);
ellipse(420,290,5);
ellipse(400,300,6);
ellipse(600,40,8);
ellipse(300,300,5);
ellipse(310,210,5);
ellipse(400,20,5);
ellipse(20,400,5);
ellipse(50,200,5);
ellipse(90,180,5);
ellipse(80,150,5);
ellipse(85,300,5);
ellipse(220,100,5);
ellipse(220,410,6)
ellipse(480,390,5);
ellipse(550,490,30);
ellipse(520,350,8);
ellipse(10,300,6);
ellipse(300,10,5);
ellipse(150,55,5);
ellipse(550,200,5);
ellipse(590,100,5);
ellipse(600,450,5);
ellipse(620,80,5);
ellipse(470,500,5);
ellipse(300,300,5);
ellipse(350,450,7);
*/
//ellipse(random(windowWidth),random(windowHeight), 7, 7);

}
