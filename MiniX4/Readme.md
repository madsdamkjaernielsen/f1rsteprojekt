[(: My project :)](https://madsdamkjaernielsen.gitlab.io/f1rsteprojekt/MiniX4/)
<br>
[My code](https://gitlab.com/madsdamkjaernielsen/f1rsteprojekt/-/blob/master/MiniX4/sketch.js)
<br>
I present to you the **"personality Calculator”**:
<br>
<img src="MiniX4shot.png" width=600>
<br>
**Submit part to Transmediale:**
 My work is called the: **”Personality Calculator”**. The program is supposed to calculate the data from the picture of your face. The program uses the webcam on your laptop to collect and scan the data of your face. The program can then define you as a person and it will tell you after its calculation. 
What you don’t know about the program is that it cannot collect the data by just seeing a picture of your face. The whole setup on the screen is fake, and the result of the program will always give you the same in the end. The program is a slightly critic of programs and its users nowadays, the program should make you consider to be more critical against programs. If you try the program without knowing about how it works you might try until you get a better result, but after a few tries you might realize the result won’t change, and you were fooled by the programmer and you might feel naive. I hope people see the program as a joke and gimmick’ish program to criticize people, and all the fake programs that pretends to use and read data.
<br>

In my process with my coding I learned to use the webcam in my program combined with the clm.tracker. This part of the program was the newest addition to my programming skills. This means it also made me struggle sometimes to get the syntaxes working. Sometimes it was a frustrating process, but I managed to make a fully working program. I also added a modulo counter which counts from 1% to 100% and it should indicate a loading time for the program to collect and gather data. Another syntax I made was the ‘START’ button and the button was setup to initiate the program. 
<br>

My main focus on this program was to make it looks like it capture all the data from you and then uses it to tell you something about yourself. The program does not really work and it can only give you 1 particularly output. After you initiate the program then it starts to load a counter and it will go on until 100%. After this it will write on your forehead how your personality is. People will always be defined as a loser, and it will never change! This will fool people to retry and never get any other output. The point of the program is as mentioned before to fool you and make you think of being more critical against other programs on the internet. Today I think there is a lot of problems with data capture and often people (including me) just check yes to all programs that ask for permission to use your data. I think often we don’t mind what is happening when we check yes to programs who want our data. We actually don’t know what they use the data to and do they give it to third-party companies. I think it is thought-provoking! 
<br>

I got my inspiration to this program through a presentation from another group in our Software Studies class. They showed us a presentation about a program called the “bergheintrainer” where you try to get access to the very famous and exclusive club Berghein in Berlin. When you use the program you meet the bouncer of the club, he asks you 3 questions and make you pass or let you in. The program is prebuilt so the questions are random and your answers does not have effect on you your success or the opposite. The program decides when you open it if you will pass or not. I was somehow inspired to make a program that “collects data” and uses it to give you an output. My project and the Berghain program all knows the answer before you interact with the program. I found it funny that you sit there and believe it actually works, but you are just fooled to trust it! And maybe this is also a little bit creepy that we are so easily fooled. 
<br>

**References:**
<br>

https://berghaintrainer.com/
