[(: My project :)](https://madsdamkjaernielsen.gitlab.io/f1rsteprojekt/MiniX5/)
<br>
[My code](https://gitlab.com/madsdamkjaernielsen/f1rsteprojekt/-/blob/master/MiniX5/sketch.js)
<br>
I present to you the **"path();”**:
<br>
<img src="MiniX5shot.png" width=600>
<br>
<br>
**Start with a blank sheet of paper. Think of at least two simple rules that you want to implement in a generative program.**
<br>
I had some other sketches, but i had to make the sketch along side with the program because not all my ideas I was able to program it on my own.
This is a photo of my final result of my sketches of a fully working program: (dont mind the 'tekst' part on the scrrenshot, that is just a editing error i made while converting the picture)
<br>
<img src="pathOG.png" width=600>
<br>

The rules in my program is very simple:
<br>

- The first rule is that I want the program to generate a path of red dots on the canvas (inspired by the travel scenes from the Indian Jones movies [Movie Scene](https://www.youtube.com/watch?v=5TY5Fp6O5iM&ab_channel=BryanOlari)). Every time the path hits an obstacle it will mark the traveled path with a “defined” black line. 
<br>

- Second rule, after the red dot path hit an obstacle it will then change in a new random direction. 
(Obstacles in the program can be the boarders of the canvas or an already “defined” line). 
<br>

In the start the paths will most likely be longer because there are not many obstacles on the canvas but later there will be drawn many lines all over the canvas, and this results in the lines will become shorter and shorter. This is simply because the canvas has a limited amount of space, and because the random function choses a random path and won’t consider using the space wisely. After some time, the rules will make such many paths that the canvas would end up filled up so the background is barely visible. The rules are the running factor in my program because you cannot interact with the program or manipulate with how the paths act, but you are able to speed up the process! I made a for-loop to control the speed of the paths. If you take your mouse and place it on the canvas the further it is close to x=0 it will go slower and if you move it towards a higher y-value the speed will increase. This was actually not a part of my thoughts in the original idea, but I was tired of waiting for the lines to fill the canvas and then I just liked the feature that you have no power in how the paths are drawn but you can choose the speed.  
<br>

**Do you have any further thoughts on the theme of this chapter?**
<br>
My closing thoughts about auto generated art is very often controlled by some kind of randomness, where you as an artist makes the computer take some minor or major decisions. However, the computer does not calculate the decisions they are always chosen random. As a human artist, we might look at our canvas and carefully think were to place our next brush stroke to get the ultimate best result. The computer will never make these thoughts. 
This might bring ups the discussion is it the person who used the computer or the computer who are the artist when it comes to programmed generative art. In my opinion this is a very hard question to answer but usually you will give the credit to the human (programmer/artist) behind the piece of art. If you see recently there is a lot of people who makes more and more art that rely on random outcome. My Facebook recent years shows me videos of people hanging paint buckets in a wire over the canvas and then let it circulate and make a pattern [LINK](https://www.youtube.com/watch?v=ZOD7HQOnKAE&ab_channel=ArtInsider). Another thing is people dropping paint on the canvas which is placed on a spinning surface which makes a fancy art but you as a designer has no influence on the outcome [LINK2](https://www.youtube.com/watch?v=edCgBIdXI6w&ab_channel=JohnnyQ). These painting ways are methods I think is very close to the way the computer randomly operates in generative art.   
<br>

**References**
<br>

- Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 119-146.
<br>

- [Link: The Coding Train ](https://www.youtube.com/watch?v=bEyTZ5ZZxZs&t=301s&ab_channel=TheCodingTrain)
<br>

- [Link: Indiana Jones Travel Map](https://www.youtube.com/watch?v=5TY5Fp6O5iM&ab_channel=BryanOlari) i could not find the very accurate video but i think you get the idea
<br>

- [The Art references 1](https://www.youtube.com/watch?v=ZOD7HQOnKAE&ab_channel=ArtInsider)
<br>

- [The Art references 2](https://www.youtube.com/watch?v=edCgBIdXI6w&ab_channel=JohnnyQ)


