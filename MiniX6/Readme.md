<br>

[:) Link to my program :)](https://madsdamkjaernielsen.gitlab.io/f1rsteprojekt/MiniX6/)
<br>
[Link to my code](https://gitlab.com/madsdamkjaernielsen/f1rsteprojekt/-/blob/master/MiniX6/sketch.js)
<br>
<img src="MiniX6shot.png" width=600>
<br>

**Which MiniX do you plan to rework?**
<br>
I decided to revisit my MiniX4 “Capture All” and it was a little bit hard for me to choose which program I should chose, mainly because I got to programs which is made with critical undertones. The other program I had in mind was my MiniX2 which was very critical against humans who lives with Social Media facades and trying to always show you the “Ideal images”… 
<br>

However I chose to revisit the “Capture All” MiniX. 
This MiniX is also somekind of critical to humans nowadays, as I wrote in my Readme;
<br>

_“My main focus on this program was to make it looks like it capture all the data from you and then uses it to tell you something about yourself. The program does not really work and it can only give you 1 particularly output. After you initiate the program then it starts to load a counter and it will go on until 100%. After this it will write on your forehead how your personality is. People will always be defined as a loser, and it will never change! This will fool people to retry and never get any other output. The point of the program is as mentioned before to fool you and make you think of being more critical against other programs on the internet. Today I think there is a lot of problems with data capture and often people (including me) just check yes to all programs that ask for permission to use your data. I think often we don’t mind what is happening when we check yes to programs who want our data. We actually don’t know what they use the data to and do they give it to third-party companies. I think it is thought-provoking!”_
<br>

I like the program because it really shows a critical vision on how we today just fully trust what we see on the internet and how easy we can be fooled if we aren’t critical over the programs we interact with. 
<br>

**What have you changed and why?**
<br>
I haven’t changed the code or the program at all I just used it to look at critical making in particular
<br>

**How would you demonstrate aesthetic programming in your work? And what does it mean by programming as a practice, or even as a method for design?**
<br>
I think mostly my program is demonstrated with the sense of creative coding where I decide how the program looks, but also that in the meantime while (and before) I made the program you sit and hesitate how can you use the program to tell something or how can you show people a statement with your program. So for me my program is my kind of medium to show others what I mean or which thoughts I got. I know that these programs are mainly seen by buddy groups or fellow students, but If I was a world-famous programmer I could use this as my medium to express political views. In this case my program actually has a small hidden point, which is a critical view on humans and their general interaction with digital products. I like how you know got another platform to show the world your thoughts and views. Years ago, you were supposed to demonstrate or maybe publish a book, article etc. to be heard and be acknowledge by the society. Now you can use the programming as your way to communicate, and at the same time the internet is very fast to spread the word.  
The book in this course mentions that this Digital Design uses ‘Aesthetic Programming’ to rethink and wider the horizon for today’s and future problems. 
<br>

_“It should be explained that the title “aesthetic programming” actually derives from one of the undergraduate courses of the Digital Design degree program at Aarhus University in Denmark, which has been taught in parallel to a course in Software Studies since 2013. Taken together, as they were designed, these courses offer ways of thinking with software to understand wider political and aesthetic”_ (Aesthetic Programming p.14)
<br>

I can easily see how a few of my MiniX’es is used to understand how we people act in the interaction with digital world, but also how I used it to express some of the problems I find in the world. Somehow, I also find ‘Aesthetic Programming’ like exploratory programming because I also used it to explore how we are. 
<br>

**References**
<br>

- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24






