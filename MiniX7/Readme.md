[**(: Link to my game :)** ](https://madsdamkjaernielsen.gitlab.io/f1rsteprojekt/MiniX7/)
<br>
[Link to my code:](https://gitlab.com/madsdamkjaernielsen/f1rsteprojekt/-/blob/master/MiniX7/sketch.js)
<br>
[Link to my class: Bird](https://gitlab.com/madsdamkjaernielsen/f1rsteprojekt/-/blob/master/MiniX7/bird.js)
<br>
[Link to my class: Pipe](https://gitlab.com/madsdamkjaernielsen/f1rsteprojekt/-/blob/master/MiniX7/pipe.js)
<br>
<img src="loadscreenss.png" width=600>
<img src="ingamess.png" width=600>
<img src="gameoverss.png" width=600>
<br>

**Describe how does/do your game/game objects work?** 
My game is based on the template of ‘Flappy Bird’ I really like the way that the game is built on such a few rules and are easily understandable for its user. If you have never tried Flappy Bird you still get the sense of the rules in my game very fast. However, I think if you tried other minigames online, I am quite sure you tried a game with the same mechanics as this game/flappy bird. The simple mechanics/rules are to fly in between the boxes coming towards you, and if you hit them you lose.   
I personally think the concept of flying an object in between had such many opportunities to design and modify your own way. I decided to recreate the trench run scene from Star Wars IV: A New Hope [link](https://youtu.be/AA_D__HMuFw?t=359). Where the X-wings tries to fight inside the trenches and destroy the weapon of mass destruction the Death Star.
<br>

**Describe how you program the objects and their related attributes, and the methods in your game.** 
For my inspiration for this program, I used the guide from Daniel Shiffman making the game but in an older method. Which means I had to make some other syntaxes to make the game. First I saw the original video and followed the steps like it was shown in the Coding Train tutorial. After that it was necessary to make some changes to the program to make the project follow the descriptions of the MiniX tasks and to make it fit into a class-based and object-orientated program. In the original code of the program it was based on the objects defined by `functions()` but I had to make it where it was based on class and a `constructor()`.
<br>
I had to make 2 classes; the `class Bird` and the `class Pipe`. Because they are the key elements to my program, the bird is the player model we need to manage either up or down and then the pipes is like the hostile part of the game, these parts spawn from the right side with a random gap we as a player need fly through whiteout hitting the pipes. It sounds easy but the game is made with somekind of `gravity()` which makes it harder for the player to avoid the pipes. By pressing ‘spacebar’ you will jump higher when pressing it repeatedly but if you stop you will fall fast towards the bottom.
<br> 
Other things i added is a font i found on the internet, this font is true to the one they use in the original Star Wars movie and I just think it looked perfect into my Star Wars themed game. At the same time, I implemented a background which is a photo of the trench part of the Death Star from the movie. I doubled the size of the canvas and with a syntax I made it roll in the background to make it look more like you are actually flying in the game! And I really like this I feel it gives the game some extra visually. I must give credits to Jonas from my buddy group, he gave me the idea and helped med to make it possible. I just really like how you actually can see small x-wings flying on the background like the one you are controlling yourself. Visually I am just very satisfied with my own program.  
<br>

In JavaScript, we use class as a template for creating objects, and we can use them to encapsulate lots of data and code that we later use in our programs. It is also a very good tool to avoid very long and complex code. For instance, if you make a program with many of the same object but all of them don’t have to be the same size or etc. here it makes it easier for us as a programmer to keep array on all the parts instead of having thousands of lines of code.
<br>

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**
Object oriented programming is more than just code. It is also about the composite between the abstract and the concrete and it is about how objects in code have a relation to the world we live in. For my game, I copied a game made years ago where a bird is flying in between some obstacles. A bird is not very abstract for us, but what I made is from a fictive world Star Wars, we really don’t know how a space ship is flying in space or in the movie, but I think we can all agree that the gravity does not appear the same way in space like in the atmosphere around the world. So, the x-wing falling down will never happen. This makes my program a little bit unrealistic, but you should remember games are also used to entertainment, and sometimes you need to bend the rules to please the user.
<br>

**Failures in my program:**
<br>
Sometimes when you start the game you will see that the first 2 pipes spawn very close and sometimes it is impossible to pass through, I cannot fix it or tell you what is wrong but every new game has “bugs”.
<br>
here is a screenshot of how the game sometimes start and then you need to reload the site and hopefully it works then.
<br>
<img src="failurespawnss.png" width=600>
<br>

**References:**
<br>


- [Coding Challenge #31: Flappy Bird](https://www.youtube.com/watch?v=cXgA1d_E-jY&ab_channel=TheCodingTrain)
<br>

- [Star Wars inspiration](https://youtu.be/AA_D__HMuFw?t=359)
<br>





