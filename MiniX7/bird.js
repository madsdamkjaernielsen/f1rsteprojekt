class Bird {
constructor() {
  this.y = height/2;
  this.x = 64;

  this.gravity = 0.6;
  this.lift = -15;
  this.velocity = 0;
}

//Player 1 objektet - x-wing
  show() {
    //fill(255);
    //ellipse(this.x, this.y, 32, 32);
push();
    imageMode(CENTER);
    image(img,this.x,this.y, 64, 64 );
    //ellipse(this.x, this.y, 32, 32);
pop();
  }

  up() {
    this.velocity += this.lift;
  }

  update() {
    this.velocity += this.gravity;
    this.velocity *= 0.9;
    this.y += this.velocity;

//xwing cant fall under canvas or fly over
    if (this.y > height) {
    this.y = height;
    this.velocity = 0;
    }

    if (this.y < 0) {
    this.y = 0;
    this.velocity = 0;
    }
  }
}
