class Pipe {
constructor() {
  this.spacing = 175;
  this.top = random(height / 6, 3 / 4 * height);
  this.bottom = height - (this.top + this.spacing);
  this.x = width;
  this.w = 80;
  this.speed = 6 ;

  this.highlight = false;
}
//function that tells you if you fail the game
  hits(bird) {
    if (bird.y < this.top || bird.y > height - this.bottom) {
      if (bird.x > this.x && bird.x < this.x + this.w) {
        this.highlight = true;
        return true;
      }
    }
    this.highlight = false;
    return false;
  }

show() {
  push();
  strokeWeight(1)
    fill(105,105,105);
    if (this.highlight) {
      fill(255, 0, 0);

    noLoop();
pop();

push();
textFont(font);
  fill(255,0,0);
  stroke(0);
  strokeWeight(4);
  textSize(50)
  text('game over ', 170 ,230);
pop();

push();
fill(255,255,0);
textFont(font);
textSize(20);
stroke(0);
strokeWeight(3);
text('Press F5 to restart or reload page',100, 300);
pop();
    }
    rect(this.x, 0, this.w, this.top);
    rect(this.x, height - this.bottom, this.w, this.bottom);
  }

update() {
    this.x -= this.speed;
  }

offscreen() {
    if (this.x < -this.w) {
      return true;
    } else {
      return false;
    }
  }


}
