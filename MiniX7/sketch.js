let bird;
let pipes = [];
let score = 0;
let img;
let backdrop;
let backx = 0;
let start = 0;
let font


function preload() {
  img = loadImage('X-Wing_Top-removebg-preview.png');
  backdrop = loadImage('dstrench.jpg');
  font = loadFont('Starjedi.ttf');
}

function setup() {
  createCanvas(640, 480);
  bird = new Bird();
  pipes.push(new Pipe());
}

function draw() {
//print(start);
//baggrund
  image(backdrop,backx,0);
  backx -= 4;
  if (backx < -640 ) {
  backx = 0;

}
if(start === 0){
push();
fill(255,255,0);
textSize(30);
stroke(0);
strokeWeight(6);
textFont(font)
text('PRESS SPACE To START',150,220);
text('use SPACE to jump',170,270);
pop();

push();
textSize(60);
textFont(font);
stroke(0);
strokeWeight(6);
fill(255,255,0);
text('star wars',152,100);
pop();


}

bird.show();

if (start === 1) {


  for (var i = pipes.length-1; i >= 0; i--) {
    pipes[i].show();
    pipes[i].update();

    if (pipes[i].hits(bird)) {
      console.log("HIT");
    }

// if st. der sletter pipes fra array
    if (pipes[i].offscreen()) {
      pipes.splice(i, 1);

    score ++

    }
  }

  //Min point tæller
push();
  fill(255,255,0);
  textFont(font);
  textSize(30);
  stroke(0);
  strokeWeight(3);
  text('score '+ score, 265,50);
pop();

  bird.update();

// pipe generator
  if (frameCount % 40 == 0) {
    pipes.push(new Pipe());
  }

}
}
function keyPressed() {
  if (key == ' ') {
    bird.up();
start = 1

  }

}
